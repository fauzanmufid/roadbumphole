
<!DOCTYPE html>
<html>
<head>
	<title>Uji Coba</title>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
</head>
<body>
	<div id="mapid" style="width: 1280px; height: 720px"></div>

	<script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
	<script>
		

		var mymap = L.map('mapid').setView([51.505, -0.09], 13);

		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw', {
			maxZoom: 18,
			attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
				'<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
				'Imagery © <a href="http://mapbox.com">Mapbox</a>',
			id: 'mapbox.streets'
		}).addTo(mymap);

		<?php foreach ($data as $temp) { ?>
				
		L.marker([<?php echo $temp['latidude']; ?>, <?php echo $temp['longitude']; ?>]).addTo(mymap)
			<?php if($temp['status_jalan']=='1'){ ?>
			.bindPopup("<b>Polisi Tidur.</b>").openPopup();
			<?php } 
			elseif ($temp['status_jalan']=='0') { ?>
				.bindPopup("<b>Jalan Berlubang.</b>").openPopup();
			<?php }
			?>


		var popup = L.popup();

		function onMapClick(e) {
			popup
				.setLatLng(e.latlng)
				.setContent("You clicked the map at " + e.latlng.toString())
				.openOn(mymap);
		}

		mymap.on('click', onMapClick);
		<?php } ?>
	</script>
</body>
</html>
